<?php


require __DIR__ . '/../vendor/autoload.php';


use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

return function (App $app) {
    $container = $app->getContainer();
    
    
    $app->get('/[{name}]', function (Request $request, Response $response, array $args) use ($container) {
        // Sample log message
        $container->get('logger')->info("Slim-Skeleton '/' route");
        
        // Render index view
        return $container->get('renderer')->render($response, 'index.phtml', $args);
    });
        
        
        // do not use PUT or any other verb that's not traditional for IIS.  Will give you nightmares.
        $app->post( '/test/test', function (Request $request, Response $response, array $args)
        {
            
            $json = $request->getBody();
            $data = json_decode($json, true);
            
            //echo '{"testmsg": {"msg":"hello"} } ';
            
            echo(json_encode($data));
            
        });
        
};

